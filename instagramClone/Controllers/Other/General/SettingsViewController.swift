//
//  SettingsViewController.swift
//  instagramClone
//
//  Created by Marek Šrůtka on 14/10/2020.
//
import SafariServices
import UIKit

struct SettingCellModel {
    let Title: String
    let handler: (() -> Void)
}

/// View controller to show user settings
final class SettingsViewController: UIViewController {

    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    private var data = [[SettingCellModel]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureModels()
        view.backgroundColor = .systemBackground
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
    
    private func configureModels() {
        data.append([
            SettingCellModel(Title: "Editace Profilu") { [weak self] in
                self?.didTapEditProfile()
            },
            SettingCellModel(Title: "Pozvat prátelé") { [weak self] in
                self?.didTapInviteFriends()
            },
            SettingCellModel(Title: "Uložit Post") { [weak self] in
                self?.didTapSaveOriginalPosts()
            }
        ])
        
        
        
        data.append([
            SettingCellModel(Title: "Terms and Service") { [weak self] in
                self?.openURL(type: .terms)
            },
            SettingCellModel(Title: "Privacy Policy") { [weak self] in
                self?.openURL(type: .privacy)
            },
            SettingCellModel(Title: "Help / Feedback") { [weak self] in
                self?.openURL(type: .help)
            }
        ])
        
        data.append([
            SettingCellModel(Title: "Odhlásit se") { [weak self] in
                self?.didTabLogOut()
            }
        ])
    }
    
    enum SettingURLType {
        case terms, privacy, help
    }
    
    private func openURL(type: SettingURLType) {
        let urlString: String
        switch type {
        case .terms: urlString = "https://www.facebook.com/help/instagram/termsofuse"
        case .privacy: urlString = "https://help.instagram.com/519522125107875"
        case .help: urlString = "https://help.instagram.com/"
        }
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        let vc = SFSafariViewController(url: url)
        present(vc, animated: true)
        
    }
    
    private func didTapEditProfile() {
        let vc = EditProfileViewController()
        vc.title = "Upravit Profil"
        let navVC = UINavigationController(rootViewController: vc)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: true)
    }
    
    private func didTapInviteFriends() {
        // show share sheet to invite friend
    }
    
    private func didTapSaveOriginalPosts() {
        
    }
    
    private func didTabLogOut() {
        let actionSheet = UIAlertController(title: "Odhlášení", message: "Opravdu se chcete odhlásit?", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Zrušit", style: .cancel, handler: nil))
        actionSheet.addAction(UIAlertAction(title: "Odhlásit", style: .destructive, handler: {_ in
            AuthManager.shared.logOut(completion: { success in
                DispatchQueue.main.async {
                    if success {
                        //Present login
                        let loginVC = LoginViewController()
                        loginVC.modalPresentationStyle = .fullScreen
                        self.present(loginVC, animated: true){
                            self.navigationController?.popToRootViewController(animated: false)
                            self.tabBarController?.selectedIndex = 0
                        }
                    }
                    else {
                        //error
                        fatalError("Nelze odhlásit uživatele")
                    }
                }
             })
        }))
        actionSheet.popoverPresentationController?.sourceView = tableView
        actionSheet.popoverPresentationController?.sourceRect = tableView.bounds
        present(actionSheet, animated: true)
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = data[indexPath.section][indexPath.row].Title
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        data[indexPath.section][indexPath.row].handler()
    }
}
