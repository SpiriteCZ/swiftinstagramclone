//
//  DatabaseManager.swift
//  instagramClone
//
//  Created by Marek Šrůtka on 14/10/2020.
//

import FirebaseDatabase

public class DatabaseManager {
    
    static let shared = DatabaseManager()
    
    private let database = Database.database().reference()
    
    //MARK: - Public
    
    /// Check is email and username is available
    /// - Parameters
    ///     - email: String representing email
    ///     - username: String represiting username
    public func canCreateNewUser(with email: String, username: String, completion: (Bool) -> Void) {
        completion(true)
    }
    
    /// Insert new user data to databse
    /// - Parameters
    ///     - email: String representing email
    ///     - username: String represiting username
    ///     - completion: Async callback for resultif database entry  succeeded
    public func insertNewUser(with email: String, username: String, completion: @escaping (Bool) -> Void){
        database.child(email.safeDatabaseKey()).setValue(["username": username]){ error, _  in
            if error == nil {
                //succeeded
                completion(true)
                return
            }
            else {
                 //failed
                completion(false)
                return
            }
        }
    }
}
